# CHANNEL=A1,A2,B1,B2,C1,C2
# n=1..6

record(longin, "${P}${R}${CHANNEL}:n") {
    field(DESC, "The serial protocol channel identifier")
    field(DISP, "1")
    field(PINI, "YES")
    field(VAL,  "${n}")
}


record(stringin, "${P}${R}${CHANNEL}:ChanR") {
    field(DESC, "The channel this gauge is connected to")
    field(DISP, "1")
    field(PINI, "YES")
    field(VAL,  "${CHANNEL}")
}


record(calcout, "${P}${R}${CHANNEL}:iRereadCalc") {
    field(INPA, "${P}${R}${CHANNEL}:ValidR CP")
    field(CALC, "A")
    field(OUT,  "${P}${R}${CHANNEL}:RereadEvent.PROC PP")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")
}


record(event, "${P}${R}${CHANNEL}:RereadEvent") {
# This PV is an extension of CommsOK: it also triggers processing
#  every time iRereaderCommsOK is processed which is roughly once in a minute
    field(DESC, "PV to enable/disable/trigger comms")
}


# To be populated by actual gauges
record(fanout, "${P}${R}${CHANNEL}:iInit-FO") {
    field(SCAN, "Event")
}


record(bi, "${P}${R}${CHANNEL}:ValidR") {
    field(ZNAM, "Incorrect Gauge Type")
    field(ONAM, "Correct Gauge Type")
    field(ZSV,  "MAJOR")
    field(FLNK, "${P}${R}${CHANNEL}:iRereadCalc")
}


record(scalcout, "${P}${R}${CHANNEL}:iCheckType") {
    field(INAA, "${P}${R}${CHANNEL}:SensorTypeR CP")
    field(INPA, "${P}${R}CommsOK CP")
    field(CALC, "'0'")
    field(OOPT, "On Change")
    field(OUT,  "${P}${R}${CHANNEL}:ValidR PP")
}


#To be filled by actual gauge databases
record(stringin, "${P}${R}${CHANNEL}:DevNameR") {
    field(DESC, "Devicename of gauge connected to channel")
    field(DISP, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}${CHANNEL}:ValidR CP")
}


record(longin, "${P}${R}${CHANNEL}:NumOfRlysR") {
    field(DESC, "Number of relays assigned to channel")
    field(DISP, "1")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}${CHANNEL}:ValidR CP")
}
